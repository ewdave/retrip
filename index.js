import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import logger from 'morgan';

const app = express();

app.set('port', process.env.PORT || 3000);
app.enable('trust proxy');
app.disable('x-powered-by');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));

// Set middleware headers
// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', '*');
//   res.header('Access-Control-Allow-Methods', 'GET');
//   next();
// });

app.use((req, res, next) => {
  req.method != 'GET' ? res.send('') : next();
});

app.get('/', (req, res) => {
  const ip =
    req.ip ||
    req.ips ||
    req.headers['X-Forwarded-For'] ||
    req.connection.remoteAddress;

  const reqQuery = req.query.format;
  switch (reqQuery) {
    case 'json':
      res.status(200).json({ ip });
      break;
    case 'jsonp':
      const callback = req.query.callback ? req.query.callback : 'callback';
      res.set('Content-Type', 'application/javascript');
      res.send(`${callback}({"ip":"${ip}"})`);
      break;
    default:
      res.set('Content-Type', 'text/plain')
      res.status(200).send(ip);
  }
});

app.get('*', (req, res) => {
  res.status(404).send('Not Found');
});

const server = http.createServer(app).listen(app.get('port'), () => {
  console.log('Server running on port ', app.get('port'));
});
